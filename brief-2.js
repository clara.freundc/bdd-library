INSERT INTO book (title, description, release_date, authors, available) 
VALUES ("Moby Dick","The book is the sailor Ishmael's narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge against Moby Dick, the giant white whale that on the ship's previous voyage bit off Ahab's leg at the knee.","1851-10-18","Herman Melville »,1);

INSERT INTO book(title, description, release_date, authors, available)
VALUES
("The Great Gatsby", "Set in Jazz Age New York, it tells the tragic story of Jay Gatsby, a self-made millionaire, and his pursuit of Daisy Buchanan, a wealthy young woman whom he loved in his youth.","1925-04-10", "F. Scott Fitzgerald", 1),
("Frankenstein", "Victor Frankenstein tells Walton his story—a happy childhood, an unhealthy obsession with alchemy, and his engagement to his cousin Elizabeth. Victor enrolls at the University of Ingolstadt, where he discovers the secret of life and builds a creature from dead bodies.", "1818-01-01", "Mary Shelley", 0),
("Robinson Crusoe", NULL, "1719-04-25", "Daniel Defoe", 1);

mysql> SELECT title FROM book;
+------------------+
| title            |
+------------------+
| Moby Dick        |
| The Great Gatsby |
| Frankenstein     |
| Robinson Crusoe  |
+------------------+
4 rows in set (0,00 sec)

mysql> DESC book
    -> ;
+--------------+--------------+------+-----+---------+----------------+
| Field        | Type         | Null | Key | Default | Extra          |
+--------------+--------------+------+-----+---------+----------------+
| book_id      | int          | NO   | PRI | NULL    | auto_increment |
| title        | varchar(255) | NO   |     | NULL    |                |
| description  | text         | YES  |     | NULL    |                |
| release_date | date         | NO   |     | NULL    |                |
| authors      | varchar(255) | NO   |     | NULL    |                |
| available    | tinyint(1)   | YES  |     | 1       |                |
+--------------+--------------+------+-----+---------+----------------+
6 rows in set (0,00 sec)

mysql> SELECT * FROM book WHERE authors = 'Herman Melville';
+---------+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
| book_id | title     | description                                                                                                                                                                                                                            | release_date | authors         | available |
+---------+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
|       1 | Moby Dick | The book is the sailor Ishmael's narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge against Moby Dick, the giant white whale that on the ship's previous voyage bit off Ahab's leg at the knee. | 1851-10-18   | Herman Melville |         1 |
+---------+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
1 row in set (0,00 sec)

mysql> SELECT * FROM book WHERE description IS NULL;
+---------+-----------------+-------------+--------------+--------------+-----------+
| book_id | title           | description | release_date | authors      | available |
+---------+-----------------+-------------+--------------+--------------+-----------+
|       4 | Robinson Crusoe | NULL        | 1719-04-25   | Daniel Defoe |         1 |
+---------+-----------------+-------------+--------------+--------------+-----------+
1 row in set (0,00 sec)

mysql> SELECT * FROM book WHERE release_date < '1900-01-01';
+---------+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
| book_id | title           | description                                                                                                                                                                                                                                                                    | release_date | authors         | available |
+---------+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
|       1 | Moby Dick       | The book is the sailor Ishmael's narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge against Moby Dick, the giant white whale that on the ship's previous voyage bit off Ahab's leg at the knee.                                         | 1851-10-18   | Herman Melville |         1 |
|       3 | Frankenstein    | Victor Frankenstein tells Walton his story—a happy childhood, an unhealthy obsession with alchemy, and his engagement to his cousin Elizabeth. Victor enrolls at the University of Ingolstadt, where he discovers the secret of life and builds a creature from dead bodies.   | 1818-01-01   | Mary Shelley    |         0 |
|       4 | Robinson Crusoe | NULL                                                                                                                                                                                                                                                                           | 1719-04-25   | Daniel Defoe    |         1 |
+---------+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
3 rows in set (0,00 sec)

mysql> UPDATE book
    -> SET description = 'Crusoe is on a ship bound for Africa, where he plans to buy slaves for his plantations in South America, when the ship is wrecked on an island and Crusoe is the only survivor. Alone on a desert island, Crusoe manages to survive thanks to his pluck and pragmatism.'
    -> WHERE title = 'Robinson Crusoe';
Query OK, 1 row affected (0,00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM book;
+---------+------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+---------------------+-----------+
| book_id | title            | description                                                                                                                                                                                                                                                                    | release_date | authors             | available |
+---------+------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+---------------------+-----------+
|       1 | Moby Dick        | The book is the sailor Ishmael's narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge against Moby Dick, the giant white whale that on the ship's previous voyage bit off Ahab's leg at the knee.                                         | 1851-10-18   | Herman Melville     |         1 |
|       2 | The Great Gatsby | Set in Jazz Age New York, it tells the tragic story of Jay Gatsby, a self-made millionaire, and his pursuit of Daisy Buchanan, a wealthy young woman whom he loved in his youth.                                                                                               | 1925-04-10   | F. Scott Fitzgerald |         1 |
|       3 | Frankenstein     | Victor Frankenstein tells Walton his story—a happy childhood, an unhealthy obsession with alchemy, and his engagement to his cousin Elizabeth. Victor enrolls at the University of Ingolstadt, where he discovers the secret of life and builds a creature from dead bodies.   | 1818-01-01   | Mary Shelley        |         0 |
|       4 | Robinson Crusoe  | Crusoe is on a ship bound for Africa, where he plans to buy slaves for his plantations in South America, when the ship is wrecked on an island and Crusoe is the only survivor. Alone on a desert island, Crusoe manages to survive thanks to his pluck and pragmatism.        | 1719-04-25   | Daniel Defoe        |         1 |
+---------+------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+---------------------+-----------+
4 rows in set (0,00 sec)

mysql> SELECT *
    -> FROM book
    -> WHERE description LIKE '%ship%';
+---------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
| book_id | title           | description                                                                                                                                                                                                                                                             | release_date | authors         | available |
+---------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
|       1 | Moby Dick       | The book is the sailor Ishmael's narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge against Moby Dick, the giant white whale that on the ship's previous voyage bit off Ahab's leg at the knee.                                  | 1851-10-18   | Herman Melville |         1 |
|       4 | Robinson Crusoe | Crusoe is on a ship bound for Africa, where he plans to buy slaves for his plantations in South America, when the ship is wrecked on an island and Crusoe is the only survivor. Alone on a desert island, Crusoe manages to survive thanks to his pluck and pragmatism. | 1719-04-25   | Daniel Defoe    |         1 |
+---------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+-----------------+-----------+
2 rows in set (0,00 sec)

mysql> DELETE FROM `book`
    -> WHERE available IS FALSE;
Query OK, 1 row affected (0,00 sec)

mysql> SELECT * FROM book;
+---------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+---------------------+-----------+
| book_id | title            | description                                                                                                                                                                                                                                                             | release_date | authors             | available |
+---------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+---------------------+-----------+
|       1 | Moby Dick        | The book is the sailor Ishmael's narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge against Moby Dick, the giant white whale that on the ship's previous voyage bit off Ahab's leg at the knee.                                  | 1851-10-18   | Herman Melville     |         1 |
|       2 | The Great Gatsby | Set in Jazz Age New York, it tells the tragic story of Jay Gatsby, a self-made millionaire, and his pursuit of Daisy Buchanan, a wealthy young woman whom he loved in his youth.                                                                                        | 1925-04-10   | F. Scott Fitzgerald |         1 |
|       4 | Robinson Crusoe  | Crusoe is on a ship bound for Africa, where he plans to buy slaves for his plantations in South America, when the ship is wrecked on an island and Crusoe is the only survivor. Alone on a desert island, Crusoe manages to survive thanks to his pluck and pragmatism. | 1719-04-25   | Daniel Defoe        |         1 |
+---------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+---------------------+-----------+
3 rows in set (0,00 sec)
