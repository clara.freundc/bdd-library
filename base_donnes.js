mysql > CREATE DATABASE library;
Query OK, 1 row affected(0,01 sec)

mysql > CREATE TABLE book(
    -> book_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -> title VARCHAR(255) NOT NULL,
    -> description TEXT,
    -> release_date DATE NOT NULL,
    -> authors VARCHAR(255) NOT NULL,
    -> available BOOLEAN DEFAULT true
    -> );
Query OK, 0 rows affected(0,01 sec)



